<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.login" var="login" />
<fmt:message bundle="${loc}" key="local.password" var="password" />
<fmt:message bundle="${loc}" key="local.enter" var="enter" />

<fmt:message bundle="${loc}" key="local.please_sign_in" var="please_sign_in" />
<fmt:message bundle="${loc}" key="local.smth_wrong" var="smth_wrong" />

<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title>Login</title>


</head>

<body>

	<div class="container">


      <c:set var="last_command" scope="page" value="to_login_page_command"/>
      <%@include file="WEB-INF/jsp/includes/header.jsp" %>
      
	<c:if test="${user_not_found eq 'error'}">
		<div style="width: 40%; margin: 0 auto; text-align: center;">
        	<h1>${smth_wrong}</h1>
		</div>
	</c:if>
	
		<div class="container">
			<div style="width: 40%; margin: 0 auto; text-align: center;">
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="login" />
					<h2 class="form-signin-heading">${please_sign_in}</h2>
					<input type="text" name="email" class="form-control" placeholder="Email address" required autofocus> 
					<input type="password" name="password" class="form-control" placeholder="Password" required>
					<button class="btn btn-lg btn-primary btn-block" type="submit">${enter}</button>
				</form>
			</div>
		</div>
	</div>
		
</body>


</html>
