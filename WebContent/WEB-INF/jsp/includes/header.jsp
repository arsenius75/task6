<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.locbutton.name.ru"
	var="ru_button" />
<fmt:message bundle="${loc}" key="local.locbutton.name.en"
	var="en_button" />

<fmt:message bundle="${loc}" key="local.logout"	var="logout" />
<fmt:message bundle="${loc}" key="local.to_user_page" var="to_user_page" />
<fmt:message bundle="${loc}" key="local.to_admin_page" var="to_admin_page" />

<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">


</head>
<body>
<div class="page-header">
 <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" action="Controller" method="post">
				<input type="hidden" name="command" value="change_locale" />
				<input type="hidden" name="local" value="ru" />
				<button class="btn btn-success" type="submit">${ru_button}</button>
          </form>
          <form class="navbar-form navbar-right"  action="Controller" method="post">
				<input type="hidden" name="command" value="change_locale" /> 
				<input type="hidden" name="local" value="en" />
				<button class="btn btn-success" type="submit">${en_button}</button>
          </form>
          <form class="navbar-form navbar-right"  action="Controller" method="post">
				<input type="hidden" name="command" value="logout_command" /> 
				<button class="btn btn-success" type="submit">${logout}</button>
          </form>
          <c:if test="${null != sessionScope.isUser}">
		 	<form class="navbar-form navbar-right"  action="Controller" method="post">
				<input type="hidden" name="command" value="to_user_page_command" /> 
				<button class="btn btn-success" type="submit">${to_user_page}</button>
         	</form>
          </c:if>
          <c:if test="${null != sessionScope.isAdmin}">
		 	<form class="navbar-form navbar-right"  action="Controller" method="post">
				<input type="hidden" name="command" value="to_admin_page_command" /> 
				<button class="btn btn-success" type="submit">${to_admin_page}</button>
         	</form>
          </c:if>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
</div>

</body>
</html>