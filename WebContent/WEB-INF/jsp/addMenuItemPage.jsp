<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Index Page</title>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />

<fmt:message bundle="${loc}" key="local.add" var="add" />
<fmt:message bundle="${loc}" key="local.add_menu_item" var="add_menu_item" />


<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">
</head>
<body>

		<%@include file="includes/header.jsp" %>
		
	<c:if test="${cant_register eq 'error'}">
		<div style="width: 40%; margin: 0 auto; text-align: center;">
        	<h1>wrong_data</h1>
		</div>
	</c:if>
	  
		<c:if test="${info.getClass().getSimpleName() eq 'Dish' }">
		<div style="width: 40%; margin: 0 auto; text-align: center;">
				<h1><c:out value="${info.name}"></c:out></h1>
		</div>
	</c:if>
<div class="container">
<div style="width: 40%; margin: 0 auto; text-align: center;">
      <form class="form-signin" role="form" action="Controller" method="post">
      		<input type="hidden" name="command" value="add_menu_item_command" />  
        <h2 class="form-signin-heading">${add_menu_item}</h2>
        <input type="text" name="name" class="form-control" placeholder="name" required autofocus>
        <input type="text" name="time_of_preparing" class="form-control" placeholder="time of preparing" required autofocus>
        <input type="text" name="cost" class="form-control" placeholder="cost" required>
        
        <button class="btn btn-lg btn-primary btn-block" type="submit" >${add}</button>
      </form>
 </div>
    </div>

		
</body>
</html>
