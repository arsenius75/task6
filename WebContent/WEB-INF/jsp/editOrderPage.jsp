<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/helloworldTag.tld" prefix="myTag"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Users Page</title>


<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />

<fmt:message bundle="${loc}" key="local.users" var="users" />


<fmt:message bundle="${loc}" key="local.next" var="next" />
<fmt:message bundle="${loc}" key="local.previous" var="previous" />


<fmt:message bundle="${loc}" key="local.name" var="name" />

<fmt:message bundle="${loc}" key="local.time_of_preparing" var="time_of_preparing" />
<fmt:message bundle="${loc}" key="local.cost" var="cost" />
<fmt:message bundle="${loc}" key="local.count" var="count" />
<fmt:message bundle="${loc}" key="local.delete" var="delete" />
<fmt:message bundle="${loc}" key="local.orders" var="orders" />
<fmt:message bundle="${loc}" key="local.apply_order" var="apply_order" />

<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">

</head>

<body>

		<%@include file="includes/header.jsp" %>
	  
	<h2>${orders}</h2>
	<table border="1" class="table table-bordered table-striped">
		<tr>
			<td><b>${name}</b></td>
			<td><b>${time_of_preparing}</b></td>
			<td><b>${cost}</b></td>
			<td><b>${count}</b></td>
			<td><b>+/-</b></td>
		

		</tr>
		<c:forEach var="item" items="${list.keySet()}">
			<c:if test="${item.getClass().getSimpleName() eq 'Dish' }">
				<tr>
					<td><c:out value="${item.name}"></c:out></td>
					<td><c:out value="${item.timeOfPreparing}"></c:out></td>
					<td><c:out value="${item.cost}"></c:out></td>
					<td><c:out value="${list.get(item)}"></c:out></td>
					<td>
					<ul class="nav nav-pills">
					
					<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="dish_id" value="${item.dishId}" />
						<input type="hidden" name="inc" value="-" />
						<input type="hidden" name="command" value="change_dish_count_command" />
						<button class="btn btn-lg btn-primary" type="submit">-</button>
					</form>
					</li>
					<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="dish_id" value="${item.dishId}" />
						<input type="hidden" name="inc" value="+" />
						<input type="hidden" name="command" value="change_dish_count_command" />
						<button class="btn btn-lg btn-primary" type="submit">+</button>
					</form>
					</li>
					<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="dish_id" value="${item.dishId}" />
						<input type="hidden" name="inc" value="0" />
						<input type="hidden" name="command" value="change_dish_count_command" />
						<button class="btn btn-lg btn-primary" type="submit">x</button>
					</form>
					</li>
					</ul>
					</td>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	
	
	
	<div class="container" style="margin: 0 auto; text-align: center;">
		<ul class="nav nav-pills">
				<c:if test="${sessionScope.last_number > 0}">
		
			<li>
				<form class="form-signin" role="form" action="Controller" method="post">
					<input type="hidden" name="command" value="edit_order_command" />
					<input type="hidden" name="submit" value="previous" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${previous}</button>
				</form>
			</li>
			</c:if>
			<c:if test="${!list.isEmpty()}">
			
			<li>
				<form class="form-signin" role="form" action="Controller" method="post">
					<input type="hidden" name="command" value="edit_order_command" />
					<input type="hidden" name="submit" value="next" />
					<button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">${next}</button>
				</form>
			</li>
			</c:if>
			<c:if test="${null != sessionScope.isAdmin}">
				<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="command" value="apply_order_command" />
						<button class="btn btn-lg btn-primary btn-block" type="submit">${apply_order}</button>
					</form>
				</li>
			</c:if>
		</ul>
	</div>
		
</body>
</html>

