<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/helloworldTag.tld" prefix="myTag"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>User Page</title>


<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />

<fmt:message bundle="${loc}" key="local.show_menu" var="show_menu" />
<fmt:message bundle="${loc}" key="local.make_order" var="make_order" />
<fmt:message bundle="${loc}" key="local.show_orders" var="show_orders" />
<fmt:message bundle="${loc}" key="local.show_applied_orders" var="show_applied_orders" />

<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">

</head>

<body>

			    <c:set var="isUser" scope="session" value="true"/>
			    
			<%@include file="includes/header.jsp" %>
		
		<div class="container">
			<div style="width: 40%; margin: 0 auto; text-align: center;">
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="just_get_menu_command" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${show_menu}</button>
				</form>
			</div>
		</div>
		
		<div class="container">
			<div style="width: 40%; margin: 0 auto; text-align: center;">
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="make_order_command" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${make_order}</button>
				</form>
			</div>
		</div>
		
		<div class="container">
			<div style="width: 40%; margin: 0 auto; text-align: center;">
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="get_orders_command" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${show_orders}</button>
				</form>
			</div>
		</div>
		
		<div class="container">
			<div style="width: 40%; margin: 0 auto; text-align: center;">
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="get_applied_orders_command" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${show_applied_orders}</button>
				</form>
			</div>
		</div>

		
</body>
</html>