<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/helloworldTag.tld" prefix="myTag"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Menu Page</title>


<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />


<fmt:message bundle="${loc}" key="local.next" var="next" />
<fmt:message bundle="${loc}" key="local.previous" var="previous" />




<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">

</head>

<body>

	
			<%@include file="includes/header.jsp" %>

		
	<p><myTag:showMenu command_name="delete_menu_item_command" infoList="${list}" locale="${sessionScope.local}" /><p>
			
	<div class="container" style="margin: 0 auto; text-align: center;">
		<ul class="nav nav-pills">
		
			<c:if test="${sessionScope.last_number > 0}">
			<li>
				<form class="form-signin" role="form" action="Controller" method="post">
					<input type="hidden" name="command" value="just_get_menu_command" />
					<input type="hidden" name="submit" value="previous" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${previous}</button>
				</form>
			</li>
			</c:if>
			
			<c:if test="${!list.isEmpty()}">
			
			<li>
				<form class="form-signin" role="form" action="Controller" method="post">
					<input type="hidden" name="command" value="just_get_menu_command" />
					<input type="hidden" name="submit" value="next" />
					<button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">${next}</button>
				</form>
			</li>
			</c:if>
		</ul>
	</div>
	

</body>
</html>

