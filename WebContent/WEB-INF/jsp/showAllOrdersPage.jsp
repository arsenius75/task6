<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/helloworldTag.tld" prefix="myTag"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Orders Page</title>


<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />

<fmt:message bundle="${loc}" key="local.next" var="next" />
<fmt:message bundle="${loc}" key="local.previous" var="previous" />

<fmt:message bundle="${loc}" key="local.orders" var="orders" />
<fmt:message bundle="${loc}" key="local.order_number" var="order_number" />
<fmt:message bundle="${loc}" key="local.cost" var="cost" />
<fmt:message bundle="${loc}" key="local.is_paid" var="is_paid" />
<fmt:message bundle="${loc}" key="local.edit" var="edit" />
<fmt:message bundle="${loc}" key="local.delete" var="delete" />


<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">

</head>

<body>

		<%@include file="includes/header.jsp" %>
	  
	<h2>${orders}</h2>
	<table border="1" class="table table-bordered table-striped">
		<tr>
			<td><b>${order_number}</b></td>
			<td><b>${cost}</b></td>
			<td><b>${is_paid}</b></td>
			<td><b>${edit}</b></td>
			<td><b>${delete}</b></td>
		

		</tr>
		<c:forEach var="item" items="${list}">
			<c:if test="${item.getClass().getSimpleName() eq 'Order' }">
			<c:if test="${false == item.isApplied}">
			
				<tr>
					<td><c:out value="${item.orderId}"></c:out></td>
					<td><c:out value="${item.cost}"></c:out></td>
					<td><c:out value="${item.isPaid}"></c:out></td>
					<td>
					<form class="form-signin" role="form" action="Controller" method="post">
						<c:set var="current_order_id" scope="session" value="${item.orderId}"/>
						<input type="hidden" name="order_id" value="${item.orderId}" />
						<input type="hidden" name="command" value="edit_order_command" /> 
						<button class="btn btn-lg btn-primary " type="submit">${edit}</button>
					</form>
					</td>
					
					<td>
					<form class="form-signin" role="form" action="Controller" method="post">
						<c:set var="current_order_id" scope="session" value="${item.orderId}"/>
						<input type="hidden" name="order_id" value="${item.orderId}" />
						<input type="hidden" name="command" value="delete_order_command" />
						<button class="btn btn-lg btn-primary " type="submit">${delete}</button>
					</form>
					</td>
				</tr>
			</c:if>
			</c:if>
		</c:forEach>
	</table>



<div class="container" style="margin: 0 auto; text-align: center;">
		<ul class="nav nav-pills">
		<c:if test="${sessionScope.last_order_number > 0}">
		
			<li>
				<form class="form-signin" role="form" action="Controller" method="post">
					<input type="hidden" name="command" value="get_all_orders_command" />
					<input type="hidden" name="submit" value="previous" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">${previous}</button>
				</form>
			</li>
			</c:if>
			<c:if test="${!list.isEmpty()}">
				<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="command" value="get_all_orders_command" />
						<input type="hidden" name="submit" value="next" />
						<button class="btn btn-lg btn-primary btn-block" type="submit" >${next}</button>
					</form>
				</li>
			</c:if>
		</ul>
	</div>
	
</body>
</html>

