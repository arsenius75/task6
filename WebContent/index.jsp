<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/helloworldTag.tld" prefix="myTag"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Index Page</title>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.login" var="login" />
<fmt:message bundle="${loc}" key="local.password" var="password" />

<fmt:message bundle="${loc}" key="local.enter" var="enter" />
<fmt:message bundle="${loc}" key="local.register" var="register" />
<fmt:message bundle="${loc}" key="local.restaurant" var="restaurant" />
<fmt:message bundle="${loc}" key="local.register_or_login" var="register_or_login" />


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
 
 
    <link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/css/signin.css" />" rel="stylesheet">
 
</head>



 <body>
    <div class="container">
    
      <%@include file="WEB-INF/jsp/includes/header.jsp" %>
      

      <div class="jumbotron">
        <p class="lead">${register_or_login}</p>
     
        <div class="header">
        <ul class="nav nav-pills pull-center">
        <li><a class="btn btn-lg btn-success" href="login.jsp" role="button">${enter}</a></li>
        <li><p><a class="btn btn-lg btn-success" href="register.jsp" role="button">${register}</a></p></li>
        </ul>    
      
        <h3 class="text-muted">${restaurant}</h3>
      </div>
      
      </div>

   

      <div class="footer">
        <p>&copy; epam, 2015</p>
      </div>

    </div>

  </body>
  
  
</html>
