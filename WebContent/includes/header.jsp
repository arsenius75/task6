<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.login" var="login" />
<fmt:message bundle="${loc}" key="local.password" var="password" />
<fmt:message bundle="${loc}" key="local.locbutton.name.ru"
	var="ru_button" />
<fmt:message bundle="${loc}" key="local.locbutton.name.en"
	var="en_button" />
<fmt:message bundle="${loc}" key="local.enter" var="enter" />


<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/css/signin.css" />" rel="stylesheet">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">


</head>
<body>

<div class="header">
			<ul class="nav nav-pills pull-right">
				<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="command" value="change_locale" />
						<input type="hidden" name="local" value="ru" />
						<button class="btn btn-lg btn-primary btn-block" type="submit">${ru_button}</button>
						<input type="hidden" name="last_command" value="${last_command}" />
					</form>
				</li>

				<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="command" value="change_locale" /> 
						<input type="hidden" name="local" value="en" />
						<button class="btn btn-lg btn-primary btn-block" type="submit">${en_button}</button>
						<input type="hidden" name="last_command" value="${last_command}" />
					</form>
				</li>
				<li>
					<form class="form-signin" role="form" action="Controller" method="post">
						<input type="hidden" name="command" value="logout_command" /> 
						<button class="btn btn-lg btn-primary btn-block" type="submit">to index</button>
					</form>
				</li>
			</ul>
		</div>


</body>
</html>