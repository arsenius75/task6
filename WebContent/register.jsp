<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Index Page</title>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.login" var="login" />
<fmt:message bundle="${loc}" key="local.password" var="password" />
<fmt:message bundle="${loc}" key="local.register" var="register" />
<fmt:message bundle="${loc}" key="local.please_register" var="please_register" />
<fmt:message bundle="${loc}" key="local.smth_wrong" var="smth_wrong" />


<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/css/signin.css" />" rel="stylesheet">
    
    
</head>
<body>

<div class="container">


      <c:set var="last_command" scope="page" value="to_register_page_command"/>
      <%@include file="WEB-INF/jsp/includes/header.jsp" %>
      
	<c:if test="${user_not_found eq 'error'}">
		<div style="width: 40%; margin: 0 auto; text-align: center;">
        	<h1>${smth_wrong}</h1>
		</div>
	</c:if>
	
<div style="width: 40%; margin: 0 auto; text-align: center;">
      <form class="form-signin" role="form" action="Controller" method="post">
      		<input type="hidden" name="command" value="register" />  
        <h2 class="form-signin-heading">${please_register}</h2>
        <input type="text" name="email" value="" class="form-control" placeholder="Email address" required autofocus>
        <input type="text" name="login" value="" class="form-control" placeholder="Login" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        
        <button class="btn btn-lg btn-primary btn-block" type="submit" >${register}</button>
      </form>
 </div>
    </div>
    

</body>
</html>
