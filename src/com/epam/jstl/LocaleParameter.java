package com.epam.jstl;

/**
 * locale parameters
 * @author �������
 *
 */
public final class LocaleParameter { 

	private LocaleParameter(){} 

	public static final String MENU = "local.menu"; 
	public static final String NAME = "local.name"; 
	public static final String TIME_OF_PREPARING = "local.time_of_preparing"; 
	public static final String COST = "local.cost"; 
	public static final String BUTTON_NAME = "local.button_name"; 
}