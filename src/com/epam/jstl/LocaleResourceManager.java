package com.epam.jstl;

import java.util.ResourceBundle; 

/**
 * locale resource manager
 * @author �������
 *
 */
public class LocaleResourceManager { 
	private ResourceBundle bundle;
	
	public LocaleResourceManager(String locale) {
		bundle = ResourceBundle.getBundle("localization." + locale); 
	}

	public String getValue(String key){ 
		return bundle.getString(key); 
	} 
}