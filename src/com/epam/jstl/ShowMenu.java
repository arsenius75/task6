package com.epam.jstl;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.webex.dao.connectionpool.DBParameter;
import com.epam.webex.dao.connectionpool.DBResourceManager;
import com.epam.webex.entity.Dish;

/**
 * tag that shows menu table
 * @author �������
 *
 */
public class ShowMenu extends TagSupport {
	private static final long serialVersionUID = 1L;

	private List<?> infoList;
	private String command_name;
	private String button_name;

	private LocaleResourceManager lrm;

	public void setInfoList(List<?> infoList) {
		this.infoList = infoList;
	}

	public void setCommand_name(String command_name) {
		this.command_name = command_name;
	}

	public void setButton_name(String button_name) {
		this.button_name = button_name;
	}

	public void setLocale(String locale) {
		if(!"".equals(locale)){
			lrm = new LocaleResourceManager("locale_" +locale);
		}else{
			lrm = new LocaleResourceManager("locale");
		}
	}
	public int doStartTag() throws JspTagException {
		try {

			StringBuffer buffer = new StringBuffer();

			buffer.append("<h2>Menu:</h2>");
			buffer.append("<table border=\"1\" class=\"table table-bordered table-striped\">");
			buffer.append("<tr>");
			buffer.append("<td><b>")
			.append(lrm.getValue(LocaleParameter.NAME))
			.append("</b></td>");

			buffer.append("<td><b>")
			.append(lrm.getValue(LocaleParameter.TIME_OF_PREPARING))
			.append("</b></td>");

			buffer.append("<td><b>")
			.append(lrm.getValue(LocaleParameter.COST))
			.append("</b></td>");
			if (null != button_name) {
				buffer.append("<td><b>")
				.append(button_name)
				.append("</b></td>");
			}
			buffer.append("</tr>");

			for (Object item : infoList) {
				if (item.getClass().getSimpleName().equals("Dish")) {
					Dish dish = (Dish) item;
					buffer.append("<tr>").append("<td>").append(dish.getName())
					.append("</td>")

					.append("<td>").append(dish.getTimeOfPreparing())
					.append("</td>")

					.append("<td>").append(dish.getCost())
					.append("</td>");

					if (null != button_name) {
						buffer.append("<td>")

						.append("<form class=\"form-signin\" role=\"form\" action=\"Controller\" method=\"post\">")
						.append("<input type=\"hidden\" name=\"dish_id\" value=\"")
						.append(dish.getDishId())
						.append("\" />")

						.append("<input type=\"hidden\" name=\"command\" value=\"")
						.append(command_name)
						.append("\" />")

						.append("<button class=\"btn btn-lg btn-primary \" type=\"submit\" >")
						.append(button_name).append("</button>")

						.append("</form>").append("</td>");
					}
					buffer.append("</tr>");
				}
			}
			buffer.append("</table>");
			pageContext.getOut().print(buffer.toString());
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
		return SKIP_BODY;
	}
}