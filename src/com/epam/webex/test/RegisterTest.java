package com.epam.webex.test;


import org.junit.Test;

import com.epam.webex.entity.UserData;
import com.epam.webex.service.ServiceException;
import com.epam.webex.service.UserService;

import static org.junit.Assert.assertEquals;


public class RegisterTest {
	
	private UserService userService = UserService.getInstance();

	@Test
	public void testForm() throws ServiceException {
		UserData expected = new UserData();				

		expected.setEmail("uum@ail.ru");
		expected.setLogin("qqqqq");
		expected.setPassword("uu");

		userService.register(expected);
		UserData actual = userService.login(expected);
		assertEquals(actual.getEmail(), expected.getEmail());

		userService.deleteUser(actual.getUserId());
	}
}