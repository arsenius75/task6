package com.epam.webex.entity;

public class UserData {
	private int userId;
	private String login;
	private String email;
	private String password;
	private String status;
	
	public UserData(int userId, String login, String email, String password,
			String status) {
		super();
		this.userId = userId;
		this.login = login;
		this.email = email;
		this.password = password;
		this.status = status;
	}
	
	public UserData() {}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
