package com.epam.webex.entity;

public class DishList {

	private int orderId;
	private int dishId;
	private int count;
	
	public DishList(int orderId, int dishId, int count) {
		super();
		this.orderId = orderId;
		this.dishId = dishId;
		this.count = count;
	}
	
	public int getOrder_id() {
		return orderId;
	}
	public void setOrder_id(int orderId) {
		this.orderId = orderId;
	}
	public int getDish_id() {
		return dishId;
	}
	public void setDish_id(int dishId) {
		this.dishId = dishId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
