package com.epam.webex.entity;

public class Order {

	private int orderId;
	private int userId;
	private int cost;
	private boolean isPaid;
	private boolean isApplied;

	public Order(){}
	public Order(int orderId, int userId, int cost, boolean isPaid) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.cost = cost;
		this.isPaid = isPaid;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public boolean getIsPaid() {
		return isPaid;
	}
	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}
	public boolean getIsApplied() {
		return isApplied;
	}
	public void setApplied(boolean isApplied) {
		this.isApplied = isApplied;
	}
}
