package com.epam.webex.entity;

public class Dish {

	private String name;
	private int timeOfPreparing;
	private int cost;
	private int dishId;
	
	public Dish(){}
	
	public Dish(String name, int timeOfPreparing, int cost, int dishId) {
		super();
		this.name = name;
		this.timeOfPreparing = timeOfPreparing;
		this.cost = cost;
		this.dishId = dishId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTimeOfPreparing() {
		return timeOfPreparing;
	}

	public void setTime_of_preparing(int timeOfPreparing) {
		this.timeOfPreparing = timeOfPreparing;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getProductId() {
		return dishId;
	}

	public void setProductId(int dish_id) {
		this.dishId = dish_id;
	}

	public int getDishId() {
		return dishId;
	}

	public void setDishId(int dishId) {
		this.dishId = dishId;
	}
	
	
}
