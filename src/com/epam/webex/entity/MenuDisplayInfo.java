package com.epam.webex.entity;

public class MenuDisplayInfo {

	private int countOnPage;
	private int lastNumber;
	private String buttonName;
	
	public MenuDisplayInfo(int countOnPage, int lastNumber, String buttonName) {
		super();
		this.countOnPage = countOnPage;
		this.lastNumber = lastNumber;
		this.buttonName = buttonName;
	}
	public int getCountOnPage() {
		return countOnPage;
	}
	public void setCount_on_page(int countOnPage) {
		this.countOnPage = countOnPage;
	}
	public int getLastNumber() {
		return lastNumber;
	}
	public void setLastNumber(int lastNumber) {
		this.lastNumber = lastNumber;
	}
	public String getButtonName() {
		return buttonName;
	}
	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
}
