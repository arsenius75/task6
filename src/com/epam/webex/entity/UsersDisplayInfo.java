package com.epam.webex.entity;

public class UsersDisplayInfo {

	private int countOnPage;
	private int lastNumber;
	private String buttonName;
	
	public UsersDisplayInfo(int countOnPage, int lastNumber, String buttonName) {
		super();
		this.countOnPage = countOnPage;
		this.lastNumber = lastNumber;
		this.buttonName = buttonName;
	}
	public int getCountOnPage() {
		return countOnPage;
	}
	public void setCountOnPage(int countOnPage) {
		this.countOnPage = countOnPage;
	}
	public int getLastNumber() {
		return lastNumber;
	}
	public void setLastNumber(int lastNumber) {
		this.lastNumber = lastNumber;
	}
	public String getButtonName() {
		return buttonName;
	}
	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
}
