package com.epam.webex.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.CommandHelper;
import com.epam.webex.controller.command.ICommand;

/**
 * servlet controller
 * @author �������
 *
 */
@WebServlet("/Controller")
public final class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Controller() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
		String page = null;
		try {
			ICommand command = CommandHelper.getInstance().getCommand(commandName);
			page = command.execute(request);
		} catch (Exception e) {
			page = JspPageName.ERROR_PAGE;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(page);
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		} else {
			errorMessageDirectlyFromResponse(response);
		}
	}

	private void errorMessageDirectlyFromResponse(HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		response.getWriter().println("E R R O RR");
	}
}
