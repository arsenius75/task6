package com.epam.webex.controller;

/**
 * request parameter names
 * @author �������
 *
 */
public final class RequestParameterName {
    private RequestParameterName() {}

    public static final String COMMAND_NAME = "command";
//    public static final String SIMPLE_INFO = "simpleinfo";
    public static final String LIST = "list";
    public static final String PARAM_VALUE = "param_value";

    public static final String FILE_NAME = "filename";
    public static final String SUBMIT = "submit";
    public static final String INFO = "info";
    public static final String LAST_COMMAND = "last_command";
    
    public static final String EMAIL = "email";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password"; 
    public static final String USER_NOT_FOUND = "user_not_found"; 
    public static final String CANT_REGISTER = "cant_register"; 
    public static final String USER_ID = "user_id"; 
    public static final String DISH_ID = "dish_id"; 
    public static final String ORDER_ID = "order_id"; 
    public static final String LOCAL = "local";
    public static final String NAME = "name"; 
    public static final String TIME_OF_PREPARING = "time_of_preparing"; 
    public static final String COST = "cost"; 
    public static final String INC = "inc"; 
    
}