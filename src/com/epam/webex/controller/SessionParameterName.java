package com.epam.webex.controller;

/**
 * session parameter names
 * @author �������
 *
 */
public final class SessionParameterName {
	private SessionParameterName() {}

	public static final String CURRENT_ORDER_ID = "current_order_id";
	public static final String LOGGED_USER_ID = "logged_user_id";
	public static final String LOGGED_USER_STATUS = "logged_user_status";
	
	public static final String COUNT_ON_PAGE = "count_on_page";
	public static final String LAST_NUMBER = "last_number";
	public static final String ORDERS_COUNT_ON_PAGE = "orders_count_on_page";
	public static final String LAST_ORDER_NUMBER = "last_order_number";
	public static final String USERS_COUNT_ON_PAGE = "users_count_on_page";
	public static final String LAST_USER_NUMBER = "last_user_number";
	public static final String LAST_QUERY = "last_query";
	
//	public static final String CURRENT_ORDER_ID = "current_order_id";
//	public static final String CURRENT_ORDER_ID = "current_order_id";
	
}
