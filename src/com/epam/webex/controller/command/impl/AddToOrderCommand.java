package com.epam.webex.controller.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.CommandHelper;
import com.epam.webex.controller.command.CommandName;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.Dish;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.service.ServiceException;

/**
 * add menu item to order
 * @author �������
 *
 */
public final class AddToOrderCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Integer dishId = Integer.valueOf(request.getParameter(RequestParameterName.DISH_ID));
		Integer orderId = Integer.valueOf(request.getSession(true).
				getAttribute(SessionParameterName.CURRENT_ORDER_ID).toString());
		
		try {
			orderService.addDishToOrder(dishId, orderId);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}
		
		String buttonName = request.getParameter(RequestParameterName.SUBMIT);
		HttpSession session = request.getSession(true);

		if(null == session.getAttribute(SessionParameterName.COUNT_ON_PAGE)){
			session.setAttribute(SessionParameterName.COUNT_ON_PAGE, 2);
			session.setAttribute(SessionParameterName.LAST_NUMBER, 0);

		}
		int countOnPage = Integer.valueOf(session.getAttribute(SessionParameterName.COUNT_ON_PAGE).toString());
		int lastNumber = Integer.valueOf(session.getAttribute(SessionParameterName.LAST_NUMBER).toString());
		
		MenuDisplayInfo mdi = new MenuDisplayInfo(countOnPage, lastNumber, buttonName);
		List<Dish> menuList;
		try {
			menuList = menuService.getMenuList(mdi);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}

		session.setAttribute(SessionParameterName.COUNT_ON_PAGE, mdi.getCountOnPage());
		session.setAttribute(SessionParameterName.LAST_NUMBER, mdi.getLastNumber());
		
		request.setAttribute(RequestParameterName.LIST, menuList);
		return JspPageName.ORDER_MENU_PAGE;
	}
}