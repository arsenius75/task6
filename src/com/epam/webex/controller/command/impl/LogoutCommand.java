package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.service.ServiceException;

/**
 * logout
 * @author �������
 *
 */
public final class LogoutCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		request.getSession().invalidate();
		return JspPageName.INDEX_PAGE;
	}
}
