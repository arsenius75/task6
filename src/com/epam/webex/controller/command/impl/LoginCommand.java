package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.UserData;
import com.epam.webex.service.ServiceException;
import com.epam.webex.service.UserService;

/**
 * login user
 * @author �������
 *
 */
public final class LoginCommand implements ICommand{


	private final static Logger logger = Logger.getRootLogger();
    private static final int SESSION_LIFECYCLE = 600;

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String email = request.getParameter(RequestParameterName.EMAIL);
		String password = request.getParameter(RequestParameterName.PASSWORD);

		UserData userData = new UserData();
		userData.setEmail(email);
		userData.setPassword(password);

		UserData loggedUser;
		try {
			loggedUser = userService.login(userData);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}
		
		if(null != loggedUser){
			request.getSession(true).setAttribute(SessionParameterName.LOGGED_USER_ID, loggedUser.getUserId());
			request.getSession(true).setAttribute(SessionParameterName.LOGGED_USER_STATUS, loggedUser.getStatus());
			request.setAttribute(RequestParameterName.INFO, loggedUser);
			
			if(loggedUser.getStatus().equals("user")){
				return JspPageName.USER_PAGE;
			}else{
				return JspPageName.ADMIN_PAGE;
			}
		}
		else{
			logger.debug("user not found");
			request.setAttribute(RequestParameterName.USER_NOT_FOUND, "error");
			return JspPageName.LOGIN_PAGE;
		}
	}
}
