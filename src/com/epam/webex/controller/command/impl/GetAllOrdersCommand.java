package com.epam.webex.controller.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.entity.Order;
import com.epam.webex.service.ServiceException;

/**
 * get all orders
 * @author �������
 *
 */
public class GetAllOrdersCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String status = request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_STATUS).toString();
		if(null == status){
			return JspPageName.INDEX_PAGE;
		}else if(status.equals("user")){
			return JspPageName.USER_PAGE;
		}
		
		String buttonName = request.getParameter(RequestParameterName.SUBMIT);
		HttpSession session = request.getSession(true);

		if(null == session.getAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE)){
			session.setAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE, 5);
			session.setAttribute(SessionParameterName.LAST_ORDER_NUMBER, 0);

		}
		int ordersCountOnPage = Integer.valueOf(session.getAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE).toString());
		int lastOrderNumber = Integer.valueOf(session.getAttribute(SessionParameterName.LAST_ORDER_NUMBER).toString());
		
		MenuDisplayInfo mdi = new MenuDisplayInfo(ordersCountOnPage, lastOrderNumber, buttonName);
		List<Order> orderList;
		try {
			orderList = orderService.getAllOrdersList(mdi);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}

		session.setAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE, mdi.getCountOnPage());
		session.setAttribute(SessionParameterName.LAST_ORDER_NUMBER, mdi.getLastNumber());
		
		request.setAttribute(RequestParameterName.LIST, orderList);
		return JspPageName.SHOW_ALL_ORDERS_PAGE;
	}
}