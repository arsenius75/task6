package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.service.ServiceException;

public final class ToAdminPageCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		String status = request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_STATUS).toString();
		if(null == status){
			return JspPageName.INDEX_PAGE;
		}else if(status.equals("user")){
			return JspPageName.USER_PAGE;
		}
		return JspPageName.ADMIN_PAGE;
	}
}