package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.service.ServiceException;

/**
 * counts cost of order
 * @author �������
 *
 */
public final class CountOrderCostCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		HttpSession session = request.getSession(true);
		int currentOrderId = Integer.valueOf(session.getAttribute(SessionParameterName.CURRENT_ORDER_ID).toString());
		try {
			orderService.countOrderCost(currentOrderId);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}
		
		return JspPageName.USER_PAGE;
	}
}
