package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.service.ServiceException;

public final class ToLoginPageCommand  implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		return JspPageName.LOGIN_PAGE;
	}
}