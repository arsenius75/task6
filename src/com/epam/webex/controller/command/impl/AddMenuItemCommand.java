package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.Dish;
import com.epam.webex.service.ServiceException;

/**
 * add item to menu list
 * @author �������
 *
 */
public final class AddMenuItemCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String name = null;
		Integer timeOfPreparing = null;
		Integer cost = null;
		
		try{
			name = request.getParameter(RequestParameterName.NAME);
			timeOfPreparing = Integer.valueOf(request.getParameter(RequestParameterName.TIME_OF_PREPARING));
			cost = Integer.valueOf(request.getParameter(RequestParameterName.COST));
		}catch(NumberFormatException e){
			request.setAttribute(RequestParameterName.CANT_REGISTER, "error");
			return JspPageName.ADD_MENU_ITEM_PAGE;
		}
		if(null == name){
			request.setAttribute(RequestParameterName.CANT_REGISTER, "error");
			return JspPageName.ADD_MENU_ITEM_PAGE;
		}
		Dish dish = new Dish();
		dish.setName(name);
		dish.setTime_of_preparing(timeOfPreparing);
		dish.setCost(cost);

		try {
			if(menuService.addMenuItem(dish)){
				request.setAttribute(RequestParameterName.INFO, dish);
				return JspPageName.ADD_MENU_ITEM_PAGE;
			}else{
				request.setAttribute(RequestParameterName.CANT_REGISTER, "error");
				return JspPageName.ADD_MENU_ITEM_PAGE;
			}
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);	
		}

	}
}
