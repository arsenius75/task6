package com.epam.webex.controller.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.CommandHelper;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.entity.UserData;
import com.epam.webex.service.ServiceException;
import com.epam.webex.service.UserService;

/** 
 * delete user
 * @author �������
 *
 */
public final class DeleteUserCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Integer userId = Integer.valueOf(request.getParameter(RequestParameterName.USER_ID));
		
		try {
			userService.deleteUser(userId);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}
		
		String buttonName = request.getParameter(RequestParameterName.SUBMIT);
		HttpSession session = request.getSession(true);

		if(null == session.getAttribute(SessionParameterName.USERS_COUNT_ON_PAGE)){
			session.setAttribute(SessionParameterName.USERS_COUNT_ON_PAGE, 2);
			session.setAttribute(SessionParameterName.LAST_USER_NUMBER, 0);

		}
		int userCountOnPage = Integer.valueOf(session.getAttribute(SessionParameterName.USERS_COUNT_ON_PAGE).toString());
		int lastUserNumber = Integer.valueOf(session.getAttribute(SessionParameterName.LAST_USER_NUMBER).toString());
		
		MenuDisplayInfo mdi = new MenuDisplayInfo(userCountOnPage, lastUserNumber, buttonName);
		List<UserData> userList;
		try {
			userList = userService.getUsersList(mdi);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);

		}

		session.setAttribute(SessionParameterName.USERS_COUNT_ON_PAGE, mdi.getCountOnPage());
		session.setAttribute(SessionParameterName.LAST_USER_NUMBER, mdi.getLastNumber());
		
		request.setAttribute(RequestParameterName.LIST, userList);
		return JspPageName.SHOW_USERS_PAGE;
	}
}