package com.epam.webex.controller.command.impl;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.Dish;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.entity.Order;
import com.epam.webex.service.ServiceException;
import com.epam.webex.service.UserService;

/**
 * edit order
 * @author �������
 *
 */
public final class EditOrderCommand implements ICommand{
	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String buttonName = request.getParameter(RequestParameterName.SUBMIT);
		HttpSession session = request.getSession(true);

		if(null == session.getAttribute(SessionParameterName.COUNT_ON_PAGE)){
			session.setAttribute(SessionParameterName.COUNT_ON_PAGE, 2);
			session.setAttribute(SessionParameterName.LAST_NUMBER, 0);

		}
		int countOnPage = Integer.valueOf(session.getAttribute(SessionParameterName.COUNT_ON_PAGE).toString());
		int lastNumber = Integer.valueOf(session.getAttribute(SessionParameterName.LAST_NUMBER).toString());
		

		Integer orderId = Integer.valueOf(session.getAttribute(SessionParameterName.CURRENT_ORDER_ID).toString());
		
		MenuDisplayInfo mdi = new MenuDisplayInfo(countOnPage, lastNumber, buttonName);
		HashMap<Dish, Integer> menuList;
		try {
			menuList = orderService.getDishList(mdi, orderId);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}
		session.setAttribute(SessionParameterName.COUNT_ON_PAGE, mdi.getCountOnPage());
		session.setAttribute(SessionParameterName.LAST_NUMBER, mdi.getLastNumber());

		
		request.setAttribute(RequestParameterName.LIST, menuList);
		return JspPageName.EDIT_ORDER_PAGE;
	}
}