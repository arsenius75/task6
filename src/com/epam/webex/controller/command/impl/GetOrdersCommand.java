package com.epam.webex.controller.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.entity.Order;
import com.epam.webex.service.ServiceException;

/**
 * get orders
 * @author �������
 *
 */
public final class GetOrdersCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String buttonName = request.getParameter(RequestParameterName.SUBMIT);
		HttpSession session = request.getSession(true);

		if(null == session.getAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE)){
			session.setAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE, 2);
			session.setAttribute(SessionParameterName.LAST_ORDER_NUMBER, 0);

		}
		int ordersCountOnPage = Integer.valueOf(session.getAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE).toString());
		int lastOrderNumber = Integer.valueOf(session.getAttribute(SessionParameterName.LAST_ORDER_NUMBER).toString());
		
		MenuDisplayInfo mdi = new MenuDisplayInfo(ordersCountOnPage, lastOrderNumber, buttonName);
		int userId = Integer.valueOf(session.getAttribute(SessionParameterName.LOGGED_USER_ID).toString());
		List<Order> orderList;
		try {
			orderList = orderService.getOrdersList(mdi, userId);
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}

		session.setAttribute(SessionParameterName.ORDERS_COUNT_ON_PAGE, mdi.getCountOnPage());
		session.setAttribute(SessionParameterName.LAST_ORDER_NUMBER, mdi.getLastNumber());
		
		request.setAttribute(RequestParameterName.LIST, orderList);
		return JspPageName.SHOW_ORDERS_PAGE;
	}
}