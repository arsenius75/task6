package com.epam.webex.controller.command.impl;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;

/**
 * returns to error page
 * @author �������
 *
 */
public final class NoSuchCommand implements ICommand {

    public String execute(HttpServletRequest request) throws CommandException {
        return JspPageName.ERROR_PAGE;
    }
}