package com.epam.webex.controller.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.CommandHelper;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.entity.UserData;
import com.epam.webex.service.ServiceException;
import com.epam.webex.service.UserService;

/**
 * register user
 * @author �������
 *
 */
public final class RegisterCommand implements ICommand{


	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		String email = request.getParameter(RequestParameterName.EMAIL);
		String login = request.getParameter(RequestParameterName.LOGIN);
		String password = request.getParameter(RequestParameterName.PASSWORD);

		UserData userData = new UserData();
		userData.setEmail(email);
		userData.setLogin(login);
		userData.setPassword(password);
		try {
			if(userService.register(userData)){
				request.setAttribute(RequestParameterName.INFO, userData);
				
				return JspPageName.INFO_PAGE;
			}else{
				request.setAttribute(RequestParameterName.USER_NOT_FOUND, "error");
				return JspPageName.REGISTER_PAGE;
			}
		} catch (ServiceException e) {
			throw new CommandException( "An error has occurred when trying to view project.", e);
		}
	}
}
