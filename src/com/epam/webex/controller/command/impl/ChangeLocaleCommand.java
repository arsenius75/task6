package com.epam.webex.controller.command.impl;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.JspPageName;
import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;
import com.epam.webex.controller.command.CommandException;
import com.epam.webex.controller.command.CommandHelper;
import com.epam.webex.controller.command.ICommand;
import com.epam.webex.service.ServiceException;

/** 
 * change locale
 * @author �������
 *
 */
public final class ChangeLocaleCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		HttpSession session = request.getSession(true);
		System.out.println(request.getParameter(RequestParameterName.LOCAL));
		session.setAttribute(RequestParameterName.LOCAL, request.getParameter(RequestParameterName.LOCAL));		

		String query = (String) session.getAttribute(SessionParameterName.LAST_QUERY);

		if(null != query){
			return query;
		}else{
			return JspPageName.INDEX_PAGE;
		}
	}
}