package com.epam.webex.controller.command;

import com.epam.webex.controller.command.impl.AddMenuItemCommand;
import com.epam.webex.controller.command.impl.AddMenuItemPageCommand;
import com.epam.webex.controller.command.impl.AddToOrderCommand;
import com.epam.webex.controller.command.impl.ApplyOrderCommand;
import com.epam.webex.controller.command.impl.ChangeDishCountCommand;
import com.epam.webex.controller.command.impl.ChangeLocaleCommand;
import com.epam.webex.controller.command.impl.CountOrderCostCommand;
import com.epam.webex.controller.command.impl.DeleteMenuItemCommand;
import com.epam.webex.controller.command.impl.DeleteOrderCommand;
import com.epam.webex.controller.command.impl.DeleteUserCommand;
import com.epam.webex.controller.command.impl.EditOrderCommand;
import com.epam.webex.controller.command.impl.GetAllOrdersCommand;
import com.epam.webex.controller.command.impl.GetAppliedOrdersCommand;
import com.epam.webex.controller.command.impl.JustShowMenuCommand;
import com.epam.webex.controller.command.impl.LoginCommand;
import com.epam.webex.controller.command.impl.LogoutCommand;
import com.epam.webex.controller.command.impl.MakeOrderCommand;
import com.epam.webex.controller.command.impl.NoSuchCommand;
import com.epam.webex.controller.command.impl.PayOrderCommand;
import com.epam.webex.controller.command.impl.RegisterCommand;
import com.epam.webex.controller.command.impl.GetMenuCommand;
import com.epam.webex.controller.command.impl.GetOrderMenuCommand;
import com.epam.webex.controller.command.impl.GetOrdersCommand;
import com.epam.webex.controller.command.impl.GetUsersCommand;
import com.epam.webex.controller.command.impl.ToAdminPageCommand;
import com.epam.webex.controller.command.impl.ToIndexPageCommand;
import com.epam.webex.controller.command.impl.ToInfoPageCommand;
import com.epam.webex.controller.command.impl.ToLoginPageCommand;
import com.epam.webex.controller.command.impl.ToRegisterPageCommand;
import com.epam.webex.controller.command.impl.ToUserPageCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * command helper
 * @author �������
 *
 */
public final class CommandHelper {
    private static final CommandHelper instance = new CommandHelper();
    private final Map<CommandName, ICommand> commands = new HashMap<CommandName, ICommand>();
    
    public CommandHelper() {
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
        commands.put(CommandName.LOGIN, new LoginCommand());
        commands.put(CommandName.REGISTER, new RegisterCommand());
        commands.put(CommandName.CHANGE_LOCALE, new ChangeLocaleCommand());
        commands.put(CommandName.GET_MENU_COMMAND, new GetMenuCommand());
        commands.put(CommandName.GET_USERS_COMMAND, new GetUsersCommand());
        commands.put(CommandName.DELETE_USER_COMMAND, new DeleteUserCommand());
        commands.put(CommandName.DELETE_MENU_ITEM_COMMAND, new DeleteMenuItemCommand());
        commands.put(CommandName.ADD_MENU_ITEM_PAGE_COMMAND, new AddMenuItemPageCommand());
        commands.put(CommandName.ADD_MENU_ITEM_COMMAND, new AddMenuItemCommand());
        commands.put(CommandName.MAKE_ORDER_COMMAND, new MakeOrderCommand());
        commands.put(CommandName.GET_ORDER_MENU_COMMAND, new GetOrderMenuCommand());
        commands.put(CommandName.ADD_TO_ORDER_COMMAND, new AddToOrderCommand());
        commands.put(CommandName.GET_ORDERS_COMMAND, new GetOrdersCommand());
        commands.put(CommandName.COUNT_ORDER_COST_COMMAND, new CountOrderCostCommand());
        commands.put(CommandName.TO_ADMIN_PAGE_COMMAND, new ToAdminPageCommand());
        commands.put(CommandName.TO_USER_PAGE_COMMAND, new ToUserPageCommand());
        commands.put(CommandName.LOGOUT_COMMAND, new LogoutCommand());
        commands.put(CommandName.JUST_GET_MENU_COMMAND, new JustShowMenuCommand());
        commands.put(CommandName.DELETE_ORDER_COMMAND, new DeleteOrderCommand());
        commands.put(CommandName.EDIT_ORDER_COMMAND, new EditOrderCommand());
        commands.put(CommandName.TO_INDEX_PAGE_COMMAND, new ToIndexPageCommand());
        commands.put(CommandName.TO_LOGIN_PAGE_COMMAND, new ToLoginPageCommand());
        commands.put(CommandName.TO_REGISTER_PAGE_COMMAND, new ToRegisterPageCommand());
        commands.put(CommandName.CHANGE_DISH_COUNT_COMMAND, new ChangeDishCountCommand());
        commands.put(CommandName.TO_INFO_PAGE_COMMAND, new ToInfoPageCommand());
        commands.put(CommandName.APPLY_ORDER_COMMAND, new ApplyOrderCommand());
        commands.put(CommandName.GET_ALL_ORDERS_COMMAND, new GetAllOrdersCommand());
        commands.put(CommandName.GET_APPLIED_ORDERS_COMMAND, new GetAppliedOrdersCommand());
        commands.put(CommandName.PAY_ORDER_COMMAND, new PayOrderCommand());
        
    }

    public static CommandHelper getInstance() {
        return instance;
    }

    /**
     * 
     * @param commandName - name of command
     * @return ICommand object
     * @throws CommandException
     */
    public ICommand getCommand(String commandName) throws CommandException{
    	
        CommandName name = CommandName.valueOf(commandName.toUpperCase());
    	
        ICommand command;
        if ( null != name){
        	try{
            command = commands.get(name);
        	}catch(IllegalArgumentException e){
        		throw new CommandException("unknown command", e);
        	}
        } else{
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }

        return command;
    }
}