package com.epam.webex.controller.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.webex.service.MenuService;
import com.epam.webex.service.OrderService;
import com.epam.webex.service.UserService;

public interface ICommand {
	static final MenuService menuService = MenuService.getInstance();
	static final UserService userService = UserService.getInstance();
	static final OrderService orderService = OrderService.getInstance();
    public String execute(HttpServletRequest request) throws CommandException;
}