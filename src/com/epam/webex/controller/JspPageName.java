package com.epam.webex.controller;

/**
 * names of jsp pages
 * @author �������
 *
 */
public final class JspPageName {

    private JspPageName() {}
    public static final String USER_PAGE = "WEB-INF/jsp/userPage.jsp";
    public static final String ADMIN_PAGE = "WEB-INF/jsp/adminPage.jsp";
    public static final String MENU_PAGE = "WEB-INF/jsp/menuPage.jsp";
    public static final String JUST_GET_MENU_PAGE = "WEB-INF/jsp/justShowMenuPage.jsp";
    public static final String ORDER_MENU_PAGE = "WEB-INF/jsp/orderMenuPage.jsp";
    public static final String ORDERS_PAGE = "WEB-INF/jsp/ordersPage.jsp";
    public static final String EDIT_ORDER_PAGE = "WEB-INF/jsp/editOrderPage.jsp";
    public static final String PAY_ORDER_PAGE = "WEB-INF/jsp/payOrderPage.jsp";
    public static final String INFO_PAGE = "WEB-INF/jsp/infoPage.jsp";
    public static final String SHOW_USERS_PAGE = "WEB-INF/jsp/showUserPage.jsp";
    public static final String SHOW_ORDERS_PAGE = "WEB-INF/jsp/showOrdersPage.jsp";
    public static final String SHOW_ALL_ORDERS_PAGE = "WEB-INF/jsp/showAllOrdersPage.jsp";
    public static final String SHOW_APPLIED_ORDERS_PAGE = "WEB-INF/jsp/showAppliedOrdersPage.jsp";
    
    public static final String REGISTER_PAGE = "register.jsp";
    public static final String ADD_MENU_ITEM_PAGE = "WEB-INF/jsp/addMenuItemPage.jsp";
    public static final String LOGIN_PAGE = "login.jsp";
    public static final String ERROR_PAGE = "error.jsp";
    public static final String INDEX_PAGE = "index.jsp";
    
}