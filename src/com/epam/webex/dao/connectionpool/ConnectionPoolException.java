package com.epam.webex.dao.connectionpool;

/**
 * connection pool exception
 * @author �������
 *
 */
public class ConnectionPoolException extends Exception {

	private static final long serialVersionUID = 1L; 

	public ConnectionPoolException(String message, Exception e){ 
		super(message, e); 
	} 
}