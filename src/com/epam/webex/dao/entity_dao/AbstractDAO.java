package com.epam.webex.dao.entity_dao;

import com.epam.webex.dao.entity_dao.impl.DaoException;
import com.epam.webex.entity.UserData;

/**
 * contains functions available for unlogined user
 * @author �������
 *
 */
public interface AbstractDAO {
	
	public void register(String login, String email, String password) throws DaoException;
	public UserData checkUser(String email, String password) throws DaoException;
	public String getStatus(int user_id) throws DaoException;
	public boolean checkUserEmail(String email, String login) throws DaoException;
	}
