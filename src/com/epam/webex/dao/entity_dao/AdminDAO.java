package com.epam.webex.dao.entity_dao;

import java.util.List;

import com.epam.webex.dao.entity_dao.impl.DaoException;
import com.epam.webex.entity.Order;
import com.epam.webex.entity.UserData;

/**
 * contains functions available for admin
 * @author �������
 *
 */
public interface AdminDAO {

	public void addMenuItem(String name, int timeOfPreparing, int cost) throws DaoException;
	public boolean checkMenuItem(String name) throws DaoException;
	public List<UserData> getUsersList(int last_number, int count_on_page) throws DaoException;
	public void deleteUser(int user_id) throws DaoException;
	public void deleteMenuItem(int dish_id) throws DaoException;
	public void applyOrder(int order_id) throws DaoException;
	public List<Order> getAllOrdersList(int last_number, int count_on_page) throws DaoException;
}
