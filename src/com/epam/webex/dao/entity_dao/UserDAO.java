package com.epam.webex.dao.entity_dao;

import java.util.HashMap;
import java.util.List;

import com.epam.webex.dao.entity_dao.impl.DaoException;
import com.epam.webex.entity.Dish;
import com.epam.webex.entity.Order;

/**
 * contaions functions available for user
 * @author �������
 *
 */
public interface UserDAO {


	public Order makeOrder(int user_id) throws DaoException;
	public void addDishToOrder(int dish_id, int order_id) throws DaoException;
	public void deleteOrder(int order_id) throws DaoException;
	public List<Order> getOrdersList(int last_number, int count_on_page, int user_id) throws DaoException;
	public HashMap<Dish, Integer> getDishList(int last_number, int count_on_page, int order_id) throws DaoException;
	public HashMap<Dish, Integer> getFullDishList(int order_id) throws DaoException;
	public void setOrderCost(int order_id, int cost) throws DaoException;
	public void deleteDish(int dish_id, int order_id, int inc) throws DaoException;
	public List<Dish> getMenuList(int lastNumber, int countOnPage) throws DaoException;
	public void payOrder(int orderId) throws DaoException;
}
