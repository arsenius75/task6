package com.epam.webex.dao.entity_dao.impl;

import com.epam.webex.exception.ProjectException;

/**
 * dao exception
 * @author �������
 *
 */
public class DaoException extends ProjectException {


    private static final long serialVersionUID = 1L; 
    public DaoException(String msg){
        super(msg);
    }

    public DaoException(String msg, Exception e){
        super(msg, e);
    }
}
