package com.epam.webex.dao.entity_dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.webex.dao.connectionpool.ConnectionPool;
import com.epam.webex.dao.connectionpool.ConnectionPoolException;
import com.epam.webex.dao.entity_dao.UserDAO;
import com.epam.webex.entity.*;

/**
 * imlementaion of the UserDAO interface
 * @author �������
 *
 */
public class DBUserDAO implements UserDAO{

	private DBUserDAO() {}
	private final static DBUserDAO instance = new DBUserDAO();

	public static DBUserDAO getInstance() {
		return instance;
	}

	private final static Logger logger = Logger.getRootLogger();

	private final static String sqlInsertIntoOrders =  "INSERT INTO orders (user_id) VALUES (?);";
	private final static String sqlSelectLastID = "SELECT LAST_INSERT_ID()";
	private final static String sqlInsertIntoDishList =  "INSERT INTO dish_list (order_id, dish_id) VALUES (?,?);";
	private final static String sqlSelectFromDishList = "SELECT * FROM dish_list WHERE order_id= ? AND dish_id = ?"; 
	private final static String sqlUpdateDishList = "UPDATE dish_list SET count= ? WHERE order_id= ? AND dish_id = ?"; 
	private final static String sqlDeleteFromOrders = "DELETE FROM orders WHERE order_id= ?"; 
	private final static String sqlDeleteFromDishList  = "DELETE FROM dish_list WHERE order_id= ?"; 
	private final static String sqlSelectFromOrders = "SELECT * FROM orders WHERE user_id= ?"; 
	private final static String sqlSelectFromDishListAndMenu = "SELECT * FROM dish_list JOIN menu ON menu.dish_id = dish_list.dish_id WHERE order_id= ?"; 
	private final static String sqlUpdateOrders = "UPDATE orders SET cost= ? WHERE order_id= ?"; 
	private final static String sqlDeleteFromDishListByOrder = "DELETE FROM dish_list WHERE order_id= ? AND dish_id = ? ";
	private final static String sqlSelectMenu =  "SELECT * FROM menu";
	private final static String sqlPayOrder = "UPDATE orders SET is_paid = true WHERE order_id= ?;"; 



	public Order makeOrder(int user_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't make order", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps = 
				con.prepareStatement(sqlInsertIntoOrders); Statement st = con.createStatement();){
			ps.setInt(1, user_id);
			ps.executeUpdate();
			ResultSet rs = st.executeQuery(sqlSelectLastID);

			if(rs.next()){
				int order_id = rs.getInt("LAST_INSERT_ID()");
				Order order = new Order();
				order.setUserId(user_id);
				order.setOrderId(order_id);
				return order;
			}else{
				logger.debug("can't insert order");
				throw new DaoException("can't insert order");
			}
		} catch (SQLException e) {
			logger.debug("can't insert order");
			throw new DaoException("can't insert order", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't insert order in DB", e1);
		}
	}

	public void payOrder(int order_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't pay order", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlPayOrder);) {

			ps.setInt(1, order_id);
			ps.execute();

		} catch (SQLException e) {
			logger.debug("can't apply order");
			throw new DaoException("can't pay order", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't pay order", e1);
		}
	}

	public void addDishToOrder(int dish_id, int order_id) throws DaoException {

		ConnectionPool cp;
		ResultSet rs = null;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert dish in dish-list", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlInsertIntoDishList);  PreparedStatement ps1 =
				con.prepareStatement(sqlSelectFromDishList); PreparedStatement ps2 =
				con.prepareStatement(sqlUpdateDishList);) {

			ps1.setInt(1, order_id);
			ps1.setInt(2, dish_id);

			rs = ps1.executeQuery(); 

			if(rs.next()){

				int count = rs.getInt(DBPrameterNames.COUNT);
				count++;
				ps2.setInt(1, count);
				ps2.setInt(2, order_id);
				ps2.setInt(3, dish_id);
				ps2.executeUpdate();

			}else{
				ps.setInt(1, order_id);
				ps.setInt(2, dish_id);
				ps.executeUpdate();
			}

		} catch (SQLException e) {
			logger.debug("can't insert dish in dish-list" + e);
			throw new DaoException("can't insert dish in dish-list", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't insert dish in dish-list", e1);
		}
	}

	public void deleteOrder(int order_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't delete order from DB", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlDeleteFromOrders);  PreparedStatement ps1 =
				con.prepareStatement(sqlDeleteFromDishList);) {

			ps1.setInt(1, order_id);
			ps1.executeUpdate();

			ps.setInt(1, order_id);
			ps.executeUpdate();

		} catch (SQLException e) {
			logger.debug("can't delete order");
			throw new DaoException("can't delete order", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("ConnectionPoolException", e1);
		}
	}

	public List<Order> getOrdersList(int lastNumber, int countOnPage, int user_id) throws DaoException {

		ResultSet rs = null;
		Order order = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't select order from DB", e2);
		}

		List<Order> orderList = new ArrayList<Order>();

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlSelectFromOrders);) {

			ps.setInt(1, user_id);

			rs = ps.executeQuery(); 

			int count = 0;
			while (count < lastNumber && rs.next()){
				count++;
			}

			for(;count < lastNumber + countOnPage && rs.next(); count++){

				order = new Order();
				order.setOrderId(rs.getInt(DBPrameterNames.ORDER_ID));
				order.setUserId(rs.getInt(DBPrameterNames.USER_ID));
				order.setCost(rs.getInt(DBPrameterNames.COST));
				order.setPaid(rs.getBoolean(DBPrameterNames.IS_PAID));
				order.setApplied(rs.getBoolean(DBPrameterNames.IS_APPLIED));
				orderList.add(order);
			}
		} catch (SQLException e) {
			logger.debug("can't select order from DB");
			throw new DaoException("can't select order from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select order from DB", e1);
		}
		return orderList; 
	}

	public HashMap<Dish, Integer> getDishList(int lastNumber, int countOnPage, int order_id) throws DaoException {

		ResultSet rs = null;
		Dish dish = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't select dish from DB", e2);
		}

		HashMap<Dish, Integer> menuList = new HashMap<Dish, Integer>();

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlSelectFromDishListAndMenu);) {

			ps.setInt(1, order_id);
			rs = ps.executeQuery(); 

			int count = 0;
			while (count < lastNumber && rs.next()){
				count++;
			}

			for(;count < lastNumber + countOnPage && rs.next(); count++){ 
				dish = new Dish(rs.getString(DBPrameterNames.NAME), rs.getInt(DBPrameterNames.TIME_OF_PREPARING), 
						rs.getInt(DBPrameterNames.COST), rs.getInt(DBPrameterNames.DISH_ID) );
				menuList.put(dish, rs.getInt(DBPrameterNames.COUNT));
			}

		} catch (SQLException e) {
			logger.debug("can't select dish from DB");
			throw new DaoException("can't select dish from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select dish from DB", e1);
		}

		return menuList; 
	}

	public HashMap<Dish, Integer> getFullDishList(int order_id) throws DaoException {

		ResultSet rs = null;
		Dish dish = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't select dish from DB", e2);
		}

		HashMap<Dish, Integer> menuList = new HashMap<Dish, Integer>();
		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlSelectFromDishListAndMenu);) {

			ps.setInt(1, order_id);
			rs = ps.executeQuery(); 

			while(rs.next()){ 
				dish = new Dish(rs.getString(DBPrameterNames.NAME), rs.getInt(DBPrameterNames.TIME_OF_PREPARING), 
						rs.getInt(DBPrameterNames.COST), rs.getInt(DBPrameterNames.DISH_ID) );
				menuList.put(dish, rs.getInt(DBPrameterNames.COUNT));
			}

		} catch (SQLException e) {
			logger.debug("can't select dish from DB");
			throw new DaoException("can't select dish from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select dish from DB", e1);
		}

		return menuList; 
	}

	public void setOrderCost(int order_id, int cost) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't updte order", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlUpdateOrders);) {

			ps.setInt(1, cost);
			ps.setInt(2, order_id);
			ps.executeUpdate(); 

		} catch (SQLException e) {
			logger.debug("can't delete user from DB");
			throw new DaoException("can't updte order", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't updte order", e1);
		}
	}

	public void deleteDish(int dish_id, int order_id, int inc) throws DaoException {

		ResultSet rs = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't delete dish from DB", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlSelectFromDishList); PreparedStatement ps1 =
				con.prepareStatement(sqlDeleteFromDishListByOrder); PreparedStatement ps2 =
				con.prepareStatement(sqlUpdateDishList);) {

			ps.setInt(1, order_id);
			ps.setInt(2, dish_id);

			rs = ps.executeQuery(); 

			int count = 0;
			if(rs.next()){
				count = rs.getInt(DBPrameterNames.COUNT);
			}

			if(count == 1 || inc == 0){
				ps1.setInt(1, order_id);
				ps1.setInt(2, dish_id);
				ps1.executeUpdate();
			}else{
				count--;
				ps2.setInt(1, count);
				ps2.setInt(2, order_id);
				ps2.setInt(3, dish_id);

				ps2.executeUpdate();
			}

		} catch (SQLException e) {
			logger.debug("can't delete dish from DB" + e);
			throw new DaoException("can't delete dish from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't delete dish from DB", e1);
		}
	}

	public List<Dish> getMenuList(int lastNumber, int countOnPage) throws DaoException {

		ResultSet rs = null;
		Dish dish = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't select dish from DB", e2);
		}

		List<Dish> menuList = new ArrayList<Dish>();

		try(Connection con = cp.takeConnection(); Statement st = con.createStatement();) {

			rs = st.executeQuery(sqlSelectMenu);

			int count = 0;
			while (count < lastNumber && rs.next()){
				count++;
			}

			for(;count < lastNumber + countOnPage && rs.next(); count++){ 
				dish = new Dish(rs.getString(DBPrameterNames.NAME), rs.getInt(DBPrameterNames.TIME_OF_PREPARING),
						rs.getInt(DBPrameterNames.COST), rs.getInt(DBPrameterNames.DISH_ID) );
				menuList.add(dish);
			}

		} catch (SQLException e) {
			logger.debug("can't select dish from DB");
			throw new DaoException("can't select dish from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select dish from DB", e1);
		}
		return menuList; 
	}
}