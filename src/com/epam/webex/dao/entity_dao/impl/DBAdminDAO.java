package com.epam.webex.dao.entity_dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.webex.dao.connectionpool.ConnectionPool;
import com.epam.webex.dao.connectionpool.ConnectionPoolException;
import com.epam.webex.dao.entity_dao.AdminDAO;
import com.epam.webex.entity.Order;
import com.epam.webex.entity.UserData;

/**
 * imlementaion of the AdminDAO interface
 * @author �������
 *
 */
public class DBAdminDAO implements AdminDAO{
	
	private final static Logger logger = Logger.getRootLogger();
	private final static String sqlInsertIntoMenu =  "INSERT INTO menu (name, time_of_preparing, cost) VALUES (?,?,?);";
	private final static String sqlDeleteFromUsers = "DELETE FROM users WHERE user_id= ?"; 
	private final static String sqlDeleteFromMenu = "DELETE FROM menu WHERE dish_id= ?"; 
	private final static String sqlSelectUsers = "SELECT * FROM users ";
	private final static String sqlApplyOrder = "UPDATE orders SET is_applied = true WHERE order_id= ?;"; 
	private final static String sqlSelectFromOrders = "SELECT * FROM orders";
	private final static String sqlSelectFromMenu = "SELECT * FROM menu WHERE name= ?";

	private final static DBAdminDAO instance = new DBAdminDAO();

	public static DBAdminDAO getInstance() {
		return instance;
	}
	
	public void addMenuItem(String name, int time_of_preparing, int cost) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert dish in DB", e2);
		}
		try(Connection con = cp.takeConnection(); PreparedStatement ps =  con.prepareStatement(sqlInsertIntoMenu)) {

			ps.setString(1, name);
			ps.setInt(2, time_of_preparing);
			ps.setInt(3, cost);
			ps.executeUpdate();

		} catch (SQLException e) {
			logger.debug("can't insert dish in DB");
			throw new DaoException("can't insert dish in DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't insert dish in DB", e1);
		}
	}
	
	public List<UserData> getUsersList(int lastNumber, int countOnPage) throws DaoException {

		ResultSet rs = null;
		UserData userData = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't select user from DB", e2);
		}

		List<UserData> userList = new ArrayList<UserData>();

		try(Connection con = cp.takeConnection(); Statement st = con.createStatement();) {

			rs = st.executeQuery(sqlSelectUsers); 

			int count = 0;
			while (count < lastNumber && rs.next()){
				count++;
			}

			for(;count < lastNumber + countOnPage && rs.next(); count++){

				userData = new UserData();
				userData.setUserId(rs.getInt(DBPrameterNames.USER_ID));
				userData.setLogin(rs.getString(DBPrameterNames.LOGIN));
				userData.setEmail(rs.getString(DBPrameterNames.EMAIL));
				userData.setStatus(rs.getString(DBPrameterNames.STATUS));
				userList.add(userData);
			}
		} catch (SQLException e) {
			logger.debug("can't select user from DB");
			throw new DaoException("can't select user from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select user from DB", e1);
		} finally{
			try {
				rs.close();
			} catch (SQLException e) {
				logger.debug("can't select user from DB");
				throw new DaoException("can't select user from DB", e);
			}
		}

		return userList; 
	}
	
	public void deleteUser(int user_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't delete user from DB", e2);
		}
		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlDeleteFromUsers);) {

			ps.setInt(1, user_id);
			ps.executeUpdate();

		} catch (SQLException e) {
			logger.debug("can't delete user from DB");
			throw new DaoException("can't delete user from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't delete user from DB", e1);
		}
	}

	public void deleteMenuItem(int dish_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't delete dish from DB", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlDeleteFromMenu);) {

			ps.setInt(1, dish_id);
			ps.executeUpdate();

		} catch (SQLException e) {
			logger.debug("can't delete dish from DB");
			throw new DaoException("can't delete dish from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't delete dish from DB", e1);
		}
	}

	public void applyOrder(int order_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't apply order", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlApplyOrder);) {

			ps.setInt(1, order_id);
			System.out.println(ps);
			ps.execute();

		} catch (SQLException e) {
			logger.debug("can't apply order");
			throw new DaoException("can't apply order", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't apply order", e1);
		}
	}
	
	public List<Order> getAllOrdersList(int lastNumber, int countOnPage) throws DaoException {

		ResultSet rs = null;
		Order order = null;
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert user in DB", e2);
		}

		List<Order> orderList = new ArrayList<Order>();

		try(Connection con = cp.takeConnection(); Statement st = con.createStatement();) {
			
			rs = st.executeQuery(sqlSelectFromOrders); 

			int count = 0;
			while (count < lastNumber && rs.next()){
				count++;
			}

			for(;count < lastNumber + countOnPage && rs.next(); count++){

				order = new Order();
				order.setOrderId(rs.getInt(DBPrameterNames.ORDER_ID));
				order.setUserId(rs.getInt(DBPrameterNames.USER_ID));
				order.setCost(rs.getInt(DBPrameterNames.COST));
				order.setPaid(rs.getBoolean(DBPrameterNames.IS_PAID));
				order.setApplied(rs.getBoolean(DBPrameterNames.IS_APPLIED));
				
				orderList.add(order);
			}
		} catch (SQLException e) {
			logger.debug("can't select order from DB");
			throw new DaoException("can't select order from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select order from DB", e1);
		} finally{
			try {
				rs.close();
			} catch (SQLException e) {
				logger.debug("can't select order from DB");
				throw new DaoException("can't select order from DB", e);
			}
		}

		return orderList; 
	}

	public boolean checkMenuItem(String name) throws DaoException {
		
		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't select menu item from DB", e2);
		}
		ResultSet rs = null;
		try(Connection con = cp.takeConnection(); PreparedStatement ps =  con.prepareStatement(sqlSelectFromMenu)) {
			ps.setString(1, name);
			rs = ps.executeQuery(); 
			if (rs.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			logger.debug("can't select user from DB");
			throw new DaoException("can't select menu item from DB", e);		
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select menu item from DB", e1);
		}
		return true; 		
	}
}
