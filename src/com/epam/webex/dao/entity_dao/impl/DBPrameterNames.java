package com.epam.webex.dao.entity_dao.impl;

/**
 * database parameter names
 * @author �������
 *
 */
public class DBPrameterNames {

	private DBPrameterNames() {}

	public static final String USER_ID = "user_id";
	public static final String LOGIN = "login";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String STATUS = "status";
	public static final String ORDER_ID = "order_id";
	public static final String COST = "cost";
	public static final String IS_PAID = "is_paid";
	public static final String IS_APPLIED= "is_applied";
	public static final String COUNT = "count";
	public static final String NAME = "name";
	public static final String TIME_OF_PREPARING = "time_of_preparing";
	public static final String DISH_ID = "dish_id";
	//	    public static final String USER_ID = "user_id";
	//	    public static final String USER_ID = "user_id";

}
