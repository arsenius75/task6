package com.epam.webex.dao.entity_dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.webex.dao.connectionpool.ConnectionPool;
import com.epam.webex.dao.connectionpool.ConnectionPoolException;
import com.epam.webex.dao.entity_dao.AbstractDAO;
import com.epam.webex.entity.UserData;

/**
 * imlementaion of the AbstractDao interface
 * @author �������
 *
 */
public class DBAbstractDAO implements AbstractDAO{

	private final static Logger logger = Logger.getRootLogger();
	
	private final static String sql_insert_users =  "INSERT INTO users (login, email, password)" +" VALUES (?,?,?);";
	private final static String sqlSelectUsers =  "SELECT * FROM users WHERE email = ? AND password = ?";
	private final static String sqlSelectUsersEmail =  "SELECT * FROM users WHERE email = ?";
	private final static String sqlSelectUsersLogin =  "SELECT * FROM users WHERE login = ?";
	private final static String sqlSelectFromUsersByID = "SELECT * FROM users WHERE user_id= ?";

	private final static DBAbstractDAO instance = new DBAbstractDAO();

	public static DBAbstractDAO getInstance() {
		return instance;
	}
	
	public void register(String login, String email, String password) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert user in DB", e2);
		}

		try(Connection con = cp.takeConnection(); PreparedStatement ps =  con.prepareStatement(sql_insert_users)) {

			ps.setString(1, login);
			ps.setString(2, email);
			ps.setString(3, String.valueOf(password.hashCode()));
			ps.executeUpdate();

		} catch (SQLException e) {
			logger.debug("can't insert user in DB");
			throw new DaoException("can't insert user in DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't insert user in DB", e1);
		}
	}

	public UserData checkUser(String email, String password) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert user in DB", e2);
		}
		ResultSet rs = null;
		UserData data = null;
		try(Connection con = cp.takeConnection(); PreparedStatement ps =  con.prepareStatement(sqlSelectUsers)) {
			ps.setString(1, email);
			ps.setString(2, String.valueOf(password.hashCode()));

			rs = ps.executeQuery(); 
			if (rs.next()) {
				data = new UserData(rs.getInt(DBPrameterNames.USER_ID), rs.getString(DBPrameterNames.LOGIN), rs.getString(DBPrameterNames.EMAIL),
						rs.getString(DBPrameterNames.PASSWORD), rs.getString(DBPrameterNames.STATUS));
			}
		} catch (SQLException e) {
			logger.debug("can't select user from DB");
			throw new DaoException("can't select user from DB", e);		
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select user from DB", e1);
		}
		return data; 
	}
	
	public boolean checkUserEmail(String email, String login) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert user in DB", e2);
		}
		ResultSet rs = null;
		try(Connection con = cp.takeConnection(); PreparedStatement ps =  con.prepareStatement(sqlSelectUsersEmail); PreparedStatement ps1 =  con.prepareStatement(sqlSelectUsersLogin)) {
			ps.setString(1, email);
			rs = ps.executeQuery(); 
			if (rs.next()) {
				return false;
			}
			
			ps1.setString(1, login);
			rs = ps1.executeQuery(); 
			if (rs.next()) {
				return false;
			}
		} catch (SQLException e) {
			logger.debug("can't select user from DB");
			throw new DaoException("can't select user from DB", e);		
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select user from DB", e1);
		}
		return true; 
	}
	
	public String getStatus(int user_id) throws DaoException {

		ConnectionPool cp;
		try {
			cp = ConnectionPool.getInstance();
		} catch (ConnectionPoolException e2) {
			logger.debug("ConnectionPoolException" + e2);
			throw new DaoException("can't insert user in DB", e2);
		}

		ResultSet rs = null;
		try(Connection con = cp.takeConnection(); PreparedStatement ps =
				con.prepareStatement(sqlSelectFromUsersByID);) {

			ps.setInt(1, user_id);
			rs = ps.executeQuery(); 

			if(rs.next()){
				return rs.getString(DBPrameterNames.STATUS);
			}else{
				throw new DaoException("no such user");
			}

		} catch (SQLException e) {
			logger.debug("can't select user from DB");
			throw new DaoException("can't select user from DB", e);
		} catch (ConnectionPoolException e1) {
			logger.debug("ConnectionPoolException" + e1);
			throw new DaoException("can't select user from DB", e1);
		} finally{
			try {
				rs.close();
			} catch (SQLException e) {
				logger.debug("can't select user from DB");
				throw new DaoException("can't select user from DB", e);
			}
		}
	}
}
