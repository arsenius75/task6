package com.epam.webex.service;

import java.util.HashMap;
import java.util.List;

import com.epam.webex.dao.entity_dao.AdminDAO;
import com.epam.webex.dao.entity_dao.UserDAO;
import com.epam.webex.dao.entity_dao.impl.DBAdminDAO;
import com.epam.webex.dao.entity_dao.impl.DaoException;
import com.epam.webex.dao.entity_dao.impl.DBUserDAO;
import com.epam.webex.entity.Dish;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.entity.Order;

/**
 * service for orders
 * @author �������
 *
 */
public class OrderService {

	private final static UserDAO userDAO = DBUserDAO.getInstance();
	private final static AdminDAO adminDAO = DBAdminDAO.getInstance();

	private final static String next = "next";
	private final static String previous = "previous";
	
	private final static OrderService instance = new OrderService();

	private OrderService() {}

	public static OrderService getInstance() {
		return instance;
	}
	
	public void deleteOrder(Integer orderId) throws ServiceException{

		if(null != orderId){
			try {
				userDAO.deleteOrder(orderId);
			} catch (DaoException e) {
				throw new ServiceException("dao exception", e);
			}
		}
	}
	
	public Order makeOrder (int userId) throws ServiceException{

		try {
			return userDAO.makeOrder(userId);
		} catch (DaoException e) {
			throw new ServiceException("database exception", e);
		}
	}
	
	public List<Order> getOrdersList(MenuDisplayInfo mdi, int user_id) throws ServiceException{

		String buttonName = mdi.getButtonName();
		int lastNumber = mdi.getLastNumber();
		int countOnPage = mdi.getCountOnPage();

		if(previous.equals(buttonName)){
			if(lastNumber > countOnPage){
				lastNumber -= countOnPage;
			}
			else{
				lastNumber = 0;
			}
		} else if(next.equals(buttonName)){
			lastNumber += countOnPage;
		}

		mdi.setLastNumber(lastNumber);
		mdi.setCount_on_page(countOnPage);

		List<Order> orderList = null;
		try {
			orderList = userDAO.getOrdersList(lastNumber, countOnPage, user_id);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
		return orderList;
	}
	
	public void countOrderCost(int orderId) throws ServiceException{
		try {
			HashMap<Dish, Integer> dishList = userDAO.getFullDishList(orderId);
			int fullCost = 0;
			for(Dish d : dishList.keySet()){
				fullCost += d.getCost() * dishList.get(d);
			}
			userDAO.setOrderCost(orderId, fullCost);
		} catch (DaoException e) {
			throw new ServiceException("database exception", e);
		}
	}
	
	public void deleteDishFromOrder(Integer dishId, Integer orderId, Integer inc) throws ServiceException{

		try {
			userDAO.deleteDish(dishId, orderId, inc);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
	}

	public void addDishToOrder(int dishId, int orderId) throws ServiceException{

		try {
			userDAO.addDishToOrder(dishId, orderId);
		} catch (DaoException  e) {
			throw new ServiceException("database exception", e);
		}
	}
	
	public HashMap<Dish, Integer> getDishList(MenuDisplayInfo mdi, int orderId) throws ServiceException{

		String buttonName = mdi.getButtonName();
		int lastNumber = mdi.getLastNumber();
		int countOnPage = mdi.getCountOnPage();

		if(previous.equals(buttonName)){
			if(lastNumber > countOnPage){
				lastNumber -= countOnPage;
			}
			else{
				lastNumber = 0;
			}
		} else if(next.equals(buttonName)){
			lastNumber += countOnPage;
		}

		mdi.setLastNumber(lastNumber);
		mdi.setCount_on_page(countOnPage);

		HashMap<Dish, Integer> dishList = null;
		try {
			dishList = userDAO.getDishList(lastNumber, countOnPage, orderId);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
		return dishList;
	}
	
	public void applyOrder(int orderId) throws ServiceException{

		try {
			adminDAO.applyOrder(orderId);
		} catch (DaoException e) {
			throw new ServiceException("database exception", e);
		}
	}
	
	public List<Order> getAllOrdersList(MenuDisplayInfo mdi) throws ServiceException{

		String buttonName = mdi.getButtonName();
		int lastNumber = mdi.getLastNumber();
		int countOnPage = mdi.getCountOnPage();

		if(previous.equals(buttonName)){
			if(lastNumber > countOnPage){
				lastNumber -= countOnPage;
			}
			else{
				lastNumber = 0;
			}
		} else if(next.equals(buttonName)){
			lastNumber += countOnPage;
		}

		mdi.setLastNumber(lastNumber);
		mdi.setCount_on_page(countOnPage);

		List<Order> orderList = null;
		try {
			orderList = adminDAO.getAllOrdersList(lastNumber, countOnPage);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
		return orderList;
	}
	
	public void payOrder (int orderId) throws ServiceException{

		try {
			userDAO.payOrder(orderId);
		} catch (DaoException e) {
			throw new ServiceException("database exception", e);
		}
	}
}
