package com.epam.webex.service;

import com.epam.webex.exception.ProjectException;

/**
 * service exception
 * @author �������
 *
 */
public class ServiceException extends ProjectException {
    private static final long serialVersionUID = 1L;
    public ServiceException(String msg){
        super(msg);
    }

    public ServiceException(String msg, Exception e){
        super(msg, e);
    }
}