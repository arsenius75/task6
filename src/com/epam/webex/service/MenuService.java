package com.epam.webex.service;

import java.util.List;

import com.epam.webex.dao.entity_dao.UserDAO;
import com.epam.webex.dao.entity_dao.impl.DBAdminDAO;
import com.epam.webex.dao.entity_dao.impl.DaoException;
import com.epam.webex.dao.entity_dao.impl.DBUserDAO;
import com.epam.webex.entity.Dish;
import com.epam.webex.entity.MenuDisplayInfo;

/**
 * service for menu
 * @author �������
 *
 */
public class MenuService {

	private final static UserDAO userDAO = DBUserDAO.getInstance();
	private final static String next = "next";
	private final static String previous = "previous";

	private final static DBAdminDAO adminDAO = DBAdminDAO.getInstance();

	private final static MenuService instance = new MenuService();

	private MenuService() {}

	public static MenuService getInstance() {
		return instance;
	}

	public List<Dish> getMenuList(MenuDisplayInfo mdi) throws ServiceException{

		String buttonName = mdi.getButtonName();
		int lastNumber = mdi.getLastNumber();
		int countOnPage = mdi.getCountOnPage();

		if(previous.equals(buttonName)){
			if(lastNumber > countOnPage){
				lastNumber -= countOnPage;
			}
			else{
				lastNumber = 0;
			}
		} else if(next.equals(buttonName)){
			lastNumber += countOnPage;
		}


		mdi.setLastNumber(lastNumber);
		mdi.setCount_on_page(countOnPage);

		List<Dish> menuList = null;
		try {
			menuList = userDAO.getMenuList(lastNumber, countOnPage);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
		return menuList;
	}

	public void deleteDishFromMenu(Integer dishId) throws ServiceException{

		if(null != dishId){
			try {
				adminDAO.deleteMenuItem(dishId);
			} catch (DaoException e) {
				throw new ServiceException("dao exception", e);
			}
		}
	}

	public boolean addMenuItem (Dish dish) throws ServiceException{

		String name = dish.getName();
		int timeOfPreparing = dish.getTimeOfPreparing();
		int cost = dish.getCost();

		try {
			if(adminDAO.checkMenuItem(name)){
				adminDAO.addMenuItem(name, timeOfPreparing, cost);
				return true;
			}
			return false;
		} catch (DaoException e) {
			throw new ServiceException("database exception", e);
		}
	}

}
