package com.epam.webex.service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.epam.webex.dao.entity_dao.AbstractDAO;
import com.epam.webex.dao.entity_dao.AdminDAO;
import com.epam.webex.dao.entity_dao.impl.DBAbstractDAO;
import com.epam.webex.dao.entity_dao.impl.DBAdminDAO;
import com.epam.webex.dao.entity_dao.impl.DaoException;
import com.epam.webex.entity.MenuDisplayInfo;
import com.epam.webex.entity.UserData;

/**
 * service for users
 * @author �������
 *
 */
public class UserService {

	private final static AdminDAO adminDAO = DBAdminDAO.getInstance();
	private final static AbstractDAO userDAO = DBAbstractDAO.getInstance();
	private final static Logger logger = Logger.getRootLogger();

	private final static String next = "next";
	private final static String previous = "previous";

	private final static UserService instance = new UserService();

	private UserService() {}

	public static UserService getInstance() {
		return instance;
	}

	public List<UserData> getUsersList(MenuDisplayInfo mdi) throws ServiceException{

		String buttonName = mdi.getButtonName();
		int lastNumber = mdi.getLastNumber();
		int countOnPage = mdi.getCountOnPage();

		if(previous.equals(buttonName)){
			if(lastNumber > countOnPage){
				lastNumber -= countOnPage;
			}
			else{
				lastNumber = 0;
			}
		} else if(next.equals(buttonName)){
			lastNumber += countOnPage;
		}

		mdi.setLastNumber(lastNumber);
		mdi.setCount_on_page(countOnPage);

		List<UserData> userList = null;
		try {
			userList = adminDAO.getUsersList(lastNumber, countOnPage);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
		return userList;
	}

	public void deleteUser(Integer userId) throws ServiceException{

		if(null != userId){
			try {
				adminDAO.deleteUser(userId);
			} catch (DaoException e) {
				throw new ServiceException("dao exception", e);
			}
		}
	}

	public UserData login(UserData userData) throws ServiceException{

		String email = userData.getEmail();
		String password = userData.getPassword();

		UserData foundUser = null;

		try {
			foundUser = userDAO.checkUser(email, password);
		} catch (DaoException e) {
			throw new ServiceException("dao exception", e);
		}
		return foundUser;
	}

	public boolean register(UserData userData) throws ServiceException{

		String REG_EX = "([.[^@\\s]]+)@([.[^@\\s]]+)\\.([a-z]+)";
		Pattern PATTERN = Pattern.compile(REG_EX);

		String login = userData.getLogin();
		String email = userData.getEmail();
		String password = userData.getPassword();

		Matcher m = PATTERN.matcher(email);
		boolean isRegistered = false;

		if(m.matches() && null != login && null != password ){
			try {
				if(userDAO.checkUserEmail(email, login)){
					userDAO.register(login, email, password);
					isRegistered = true;
				}
			} catch (DaoException e) {
				throw new ServiceException("database exception", e);
			}
		}else{
			logger.debug("wrong email or password");
		}
		return isRegistered;
	}
}

