package com.epam.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.webex.controller.RequestParameterName;
import com.epam.webex.controller.SessionParameterName;

/**
 * Servlet Filter implementation class CharsetFilter
 */
public class CharsetFilter implements Filter {

	private String encoding;
	private ServletContext context;

	/**
	 * Default constructor. 
	 */
	public CharsetFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
		context.log("charset was set");








		if(null == request.getParameter(RequestParameterName.LOCAL)){
			StringBuilder query = new StringBuilder("Controller");
			boolean isFirstParameter = true;

			Enumeration<String> parameterNames = request.getParameterNames();

			
				while (parameterNames.hasMoreElements()) {

					String paramName = parameterNames.nextElement();
					String paramValue = request.getParameter(paramName);

					if (isFirstParameter) {
						query.append("?");
						isFirstParameter = false;
					} else {
						query.append("&");
					}
					query.append(paramName)
					.append("=")
					.append(paramValue);
				
				}
				
		HttpSession session = ((HttpServletRequest) request).getSession();
		session.setAttribute(SessionParameterName.LAST_QUERY, query.toString());
	}


		



	chain.doFilter(request, response);
}

/**
 * @see Filter#init(FilterConfig)
 */
public void init(FilterConfig fConfig) throws ServletException {
	encoding = fConfig.getInitParameter("characterEncoding");
	context = fConfig.getServletContext();
}

}
